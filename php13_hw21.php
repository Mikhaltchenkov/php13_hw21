<?php
$bookContent = file_get_contents('./phonebook.json') or die('Файл с телефонной книгой не доступен');

$phoneBook = json_decode($bookContent, true);

if (count($phoneBook) == 0) {
    echo '<h3>Телефонная книга пуста!</h3>';
} else {
    ?>
    <table width="50%">
        <thead>
        <tr>
            <th width="25%">Фамилия</th>
            <th width="15%">Имя</th>
            <th width="30%">Адрес</th>
            <th width="25%">Телефон</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($phoneBook as $row) {
            echo '<tr><td>'.$row['lastName'].'</td><td>'
                .$row['firstName'].'</td><td>'
                .$row['address'].'</td><td>'
                .$row['phoneNumber'].'</td></tr>';
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4">
                Записей: <?= count($phoneBook) ?>
            </td>
        </tr>
        </tfoot>
    </table>
    <?php
}
?>
